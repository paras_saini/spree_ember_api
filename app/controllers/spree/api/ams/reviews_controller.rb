class Spree::Api::Ams::ReviewsController < Spree::StoreController
  helper Spree::BaseHelper
  include Spree::Api::Ams::Serializable
  include Spree::Api::Ams::Requestable
  before_action :create_review, only: :create
  # save if all ok
  def create
    if @review.save
      respond_with @review
    else
      render json: { errors: @review.errors.full_messages }, status: 422
    end
  end

  private

  def create_review
    params[:review][:rating].sub!(/\s*[^0-9]*\z/, '') unless params[:review][:rating].blank?
    @review = Spree::Review.new(review_params)
    @review.user = Spree::User.where(spree_api_key: request.headers["X-Spree-Token"]).first
    @review.ip_address = request.remote_ip
    @review.locale = I18n.locale.to_s if Spree::Reviews::Config[:track_locale]
  end

  def permitted_review_attributes
    [:rating, :title, :review, :name, :show_identifier, :product_id]
  end

  def review_params
    params.require(:review).permit(permitted_review_attributes)
  end
end
