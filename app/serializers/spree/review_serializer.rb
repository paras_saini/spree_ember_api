module Spree
  class ReviewSerializer < ActiveModel::Serializer
    embed :ids, include: true
    attributes :id, :product_id, :name, :location, :rating, :title, :review, :user_id
    has_one :user, serializer: UserSerializer
  end
end