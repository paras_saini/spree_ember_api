module Spree
  ProductSerializer.class_eval do
    has_many :reviews, serializer: ReviewSerializer
  end
end